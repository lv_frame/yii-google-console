function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function openLeftNav($active) {
    if ($active) {
        $(".g-left-nav").addClass("active");
        $(".g-arrow span").removeClass("glyphicon glyphicon-menu-right");
        $(".g-arrow span").addClass("glyphicon glyphicon-menu-left");
        $(".g-content").css("left", "260px");
        setCookie('left-menu-arrow', '0');
    } else {
        $(".g-left-nav").removeClass("active");
        $(".g-arrow span").removeClass("glyphicon glyphicon-menu-left");
        $(".g-arrow span").addClass("glyphicon glyphicon-menu-right");
        $(".g-content").css("left", "60px");
        setCookie('left-menu-arrow', '1');
    }
}

$(".g-top-menu a").click(function (e) {
    $(".g-top-menu a").removeClass("active");
    $(this).addClass("active");
})
$(".g-left-menu a").click(function (e) {
    $(".g-left-menu a").removeClass("active");
    $(this).addClass("active");
})
$(".g-arrow span").click(function (e) {
    if ($(".g-left-nav").hasClass("active")) {
        openLeftNav(false);
    } else {
        openLeftNav(true);
    }
})
if ($("a[href='" + window.location.pathname + "']").length !== 0) {
    $(".g-left-menu a").removeClass("active");
    $("a[href='" + window.location.pathname + "']").addClass('active');
}
if (getCookie('left-menu-arrow') == '1') {
    openLeftNav(false);
} else {
    openLeftNav(true);
}