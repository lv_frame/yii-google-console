# 基于Bootstrap开发的仿谷歌控制台

## 参考界面如下
!["console"](https://gitee.com/lv_frame/googel-console/raw/yii/images/yii.png)

## 版本说明

| tag  | 说明                    |
|------|-----------------------|
| 3.1  | 解决了3.0的一些bug |
| 3.0  | 融合了Yii的登录模块，不过需要少量配置。 |
| v2.1 | 加入了登录页面，未加入任何框架信息     |
| v2.0 | 未加入登录页面               |

### tag:3.0
在`yii 2.0.43`的基础上开发的模板，较`v2.1`版本加入了`Yii`登录模块，架构如下：
```shell
.
├── controllers
│   └── LoginController.php
├── GoogleConsoleAsset.php
├── layout
│   ├── head.php
│   ├── left.php
│   └── main.php
├── login.html
├── models
│   ├── LoginForm.php
│   └── User.php
├── README.md
└── views
    ├── index.php
    └── login.php

```

`User.php`和`LoginForm.php`是负责登录校验的模型，这些都可以从Yii框架中找到原。 


## 安装
此版本已和Yii整合，可以通过`composer`的方式获取依赖来安装：
```
composer require lvzmen/google-console: ^3.0
```

## 使用方法
- 负责登录的控制器需要继承上述`LoginController.php`，如：

```php
namespace frontend\controllers;

use lvzmen\YiiGoogleConsole\controllers\LoginController;
use Yii;

class TestController extends LoginController
{

}
```

- 修改系统默认的登录路由
  系统默认的登录路由是`site/login`，这个配置需要通过组件改变：
```php
return [
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'loginUrl' => ['test/login'],
        ]
    ]
];
```

- 修改根路由
```php
return [
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => '/test/index'
            ],
        ],
    ],
];

```

此时已经可以渲染出框架，如果你想改变菜单内容，请看下一节。

## 参数配置
logo、应用名称、菜单、用户信息等都可以动态改变，您只需要在任何一个`params.php`文件中引入菜单栏配置：
```
<?php
return [
    'title' => 'Google Console', // 系统名称
    'google-console' => [
        // 头部导航，包括logo，应用名称，菜单和用户菜单
        "head" => [
            //"username" => "Lvzmen",
            "app_name" => [
                "label" => "Google Console",
                "url" => "#",
            ],
            "logo" => [
                "img" => "",
                "url" => "",
            ],
            "nav" => [
                [
                    "label" => "Overview",
                    "url" => "#",
                    "class" => "active"
                ],
                [
                    "label" => "Develop",
                    "url" => "#",
                    "class" => ""
                ],
                [
                    "label" => "Test",
                    "url" => "#",
                    "class" => ""
                ],
            ],
            "dropdown" => [
                ["label" => "Action", "url" => "#"],
                ["label" => "Another action", "url" => "#"],
                ["label" => "Something else here", "url" => "#"],
                "separator",
                ["label" => "Logout", "url" => "#"],
            ]
        ],
        // 左边导航栏
        "left" => [
            [
                "label" => "Project Details",
                "icon" => "search",
                "href" => "#",
                "class" => "active"
            ],
            [
                "label" => "Directory information",
                "icon" => "flash",
                "href" => "#",
                "class" => ""
            ]
        ]
    ]
];
```

> 注意： 用户名并不需要特别指定，系统会自动从Yii::$app->user->identity->username中获取，但也可以强制指定以满足特殊情况。