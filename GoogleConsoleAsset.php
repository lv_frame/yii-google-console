<?php

namespace lvzmen\YiiGoogleConsole;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class GoogleConsoleAsset extends AssetBundle
{
    public $sourcePath = '@vendor/lvzmen/yii-google-console/assets/';
    public $css = [
        'css/google.css'
    ];
    public $js = [
        "js/google.js",
    ];

    public $cssOptions = ["appendTimestamp" => true];
    public $jsOptions = ["appendTimestamp" => true];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
