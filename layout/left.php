<!--左边导航-->
<div class="g-left-nav active">
    <div class="g-left-menu">
        <?php
        if(isset(Yii::$app->params["google-console"]["left"])){
            foreach (Yii::$app->params["google-console"]["left"] as $nav){
                $icon = isset($nav["icon"]) ? $nav["icon"] : "search";
                $href = isset($nav["href"]) ? $nav["href"] : "#";
                $class = isset($nav["class"]) ? $nav["class"] : "active";
                $label = isset($nav["label"]) ? $nav["label"] : "default";
                echo <<<NAV
<a class="g-item $class" href="$href">
    <span class="glyphicon glyphicon-$icon"></span>
    <div>$label</div>
</a>
NAV;
            }
        } else {
            echo '
                <a class="g-item active" href="#">
                    <span class="glyphicon glyphicon-search"></span>
                    <div>Project Details</div>
                </a>
                <a class="g-item" href="#">
                    <span class="glyphicon glyphicon-flash" aria-hidden="true"></span>
                    <div>Directory information</div>
                </a>
                <a class="g-item" href="#">
                    <span class="glyphicon glyphicon-cd" aria-hidden="true"></span>
                    <div>Company Details</div>
                </a>
            ';
        }
        ?>

    </div>
    <div class="g-arrow">
        <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
    </div>
</div>