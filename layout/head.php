<!--头部导航-->
<div class="g-top-nav">
    <div class="g-logo">
        <?php

        if(isset(Yii::$app->params["google-console"]["head"]["logo"])){
            $logo = Yii::$app->params["google-console"]["head"]["logo"];
            $img = isset($logo["img"]) ? $logo["img"] : "/favicon.ico";
            $url = isset($logo["url"]) ? $logo["url"] : "#";
        } else {
            $img = $asset->baseUrl . "/images/logo.png";
            $url = "#";
        }
        echo "<a href='$url'><img src='$img' /></a>";
        ?>

    </div>
    <div class="g-system-name">
        <?php
        if(isset(Yii::$app->params["google-console"]["head"]["app_name"])){
            $app = Yii::$app->params["google-console"]["head"]["app_name"];
            $label = isset($app["label"]) ? $app["label"] : "Google Console";
            $url = isset($app["url"]) ? $app["url"] : "#";
        } else {
            $label = "Google Console";
            $url = isset($app["url"]) ? $app["url"] : "#";
        }
        echo "<a href='$url'>$label</a>";
        ?>

    </div>
    <div class="g-top-menu">
        <?php
        if(isset(Yii::$app->params["google-console"]["head"]["nav"])){
            foreach (Yii::$app->params["google-console"]["head"]["nav"] as $nav){
                $href = isset($nav["url"]) ? $nav["url"] : "#";
                $class = isset($nav["class"]) ? $nav["class"] : "#";
                $label = isset($nav["label"]) ? $nav["label"] : "#";
                echo "<a href='$href' class='$class'>$label</a>";
            }
        } else {
            echo '
                <a href="#" class="active">Overview</a>
                <a href="#">Develop</a>
                <a href="#">Test</a>
                <a href="#">Deploy</a>
                <a href="#">Analyze</a>
            ';
        }
        ?>

    </div>
    <div class="g-top-user">
        <div class="dropdown">
            <a class="dropdown-toggle" type="button" id="user-info" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?php
                if(Yii::$app->user->isGuest){
                    echo "Lvzmen";
                }else{
                    if(isset(Yii::$app->params["google-console"]["head"]["username"])){
                        echo Yii::$app->params["google-console"]["head"]["username"];
                    } else {
                        echo Yii::$app->user->identity->username;
                    }

                }
                ?>
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="user-info">
                <?php
                if(isset(Yii::$app->params["google-console"]["head"]["dropdown"])){
                    foreach (Yii::$app->params["google-console"]["head"]["dropdown"] as $nav){
                        if($nav == "separator"){
                            echo '<li role="separator" class="divider"></li>';
                        } else{
                            $href = isset($nav["url"]) ? $nav["url"] : "#";
                            $label = isset($nav["label"]) ? $nav["label"] : "#";
                            echo "<li><a href='$href'>$label</a></li>";
                        }
                    }
                } else {
                    echo '
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Logout</a></li>
                    ';
                }
                ?>
            </ul>
        </div>
    </div>
</div>