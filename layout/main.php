<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
$asset = lvzmen\YiiGoogleConsole\GoogleConsoleAsset::register($this);

$this->beginPage();
echo "<!doctype html>";
echo "<html lang='zh-CN'>";
// head -----------------------------------------------
echo Html::beginTag("head");
$this->registerMetaTag(["charset" => "utf-8"]);
$this->registerMetaTag(["http-equiv" => "X-UA-Compatible", "content" => "IE=edge"]);
$this->registerMetaTag(["name" => "viewport", "content" => "width=device-width, initial-scale=1"]);
$this->registerLinkTag(["rel" => "icon", "href" =>"/favicon.ico"]);
$this->registerCsrfMetaTags();
$this->registerMetaTag(['name' => 'author','content' => 'chenzhiwei']);

echo Html::tag("title", Yii::$app->params["google-console"]["title"] ?? "Demo-仿谷歌控制台-后台模板-Yii2框架最新中文教程");
$this->head();
echo Html::endTag("head");
// head done -----------------------------------------------

// body -----------------------------------------------
echo "<body>";
$this->beginBody();
?>

    <!--中间内容区: 放在第一个位置是因为头部导航的下拉框会被后覆盖-->
    <div class="g-content">
       <?=$content?>
    </div>

    <!--头部导航-->
    <?=$this->render("head", ["asset" => $asset])?>

    <!--左边导航-->
    <?=$this->render("left")?>


<?php
$this->endBody();
echo "</body>";
echo "</html>";
$this->endPage();