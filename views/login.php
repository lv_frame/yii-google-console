<?php
/* @var $this \yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$asset = \lvzmen\YiiGoogleConsole\GoogleConsoleAsset::register($this);

$this->beginPage();
echo "<!doctype html>";
echo "<html lang='zh-CN'>";
// head -----------------------------------------------
echo Html::beginTag("head");
$this->registerMetaTag(["charset" => "utf-8"]);
$this->registerMetaTag(["http-equiv" => "X-UA-Compatible", "content" => "IE=edge"]);
$this->registerMetaTag(["name" => "viewport", "content" => "width=device-width, initial-scale=1"]);
$this->registerLinkTag(["rel" => "icon", "href" =>"/favicon.ico"]);
$this->registerCsrfMetaTags();
$this->registerMetaTag(['name' => 'author','content' => 'chenzhiwei']);

echo Html::tag("title", "Login-仿谷歌控制台-后台模板-Yii2框架最新中文教程");
$this->head();
echo Html::endTag("head");
// head done -----------------------------------------------

// body -----------------------------------------------
echo "<body>";
$this->beginBody();

$usernameFieldOptions = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$passwordFieldOptions = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>


    <div class="g-login-box">
        <div class="g-login-logo">
            <?=Yii::$app->params["google-console"]["title"] ?? 'Google Console'?>
        </div>

        <div class="g-login-box-body">
            <p class="g-login-box-msg">Sign in to start your session</p>

            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

            <?= $form->field($model, 'username', $usernameFieldOptions)->label(false)->textInput(['placeholder' => 'Username']) ?>

            <?= $form->field($model, 'password', $passwordFieldOptions)->label(false)->passwordInput(['placeholder' => 'Password']) ?>

            <div class="row">
                <div class="col-xs-8">
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                </div>
                <div class="col-xs-4">
                    <?= Html::submitButton('Sign in', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>


<?php
$this->endBody();
echo "</body>";
echo "</html>";
$this->endPage();